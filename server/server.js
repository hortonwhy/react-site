//requiring express and initializing the app:
const express = require('express');
//const app = express();
const app = require('express')();
//requiering the cors middleware:
const cors = require('cors');

const PORT = 5001; //we will use port 5001

//const { MongoClient } = require('mongodb');
//const uri = "mongodb+srv://wyatt:kip@cluster0.2wata.mongodb.net/test?retryWrites=true&w=majority";
//const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
//client.connect(err => {
//  const collection = client.db("test").collection("devices");
  // perform actions on the collection object
//  client.close();
//});

const { MongoClient } = require('mongodb');
const uri = "mongodb+srv://wyatt:n3rTig6hMUUtZael@cluster0.2wata.mongodb.net/test?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

app.use(cors());//telling express to use the cors middleware

app.get('/', (req,res)=>{ //listen to a get request
	client.connect(async err => {
		const collection = client.db("test").collection("devices");

		// find everything in the collection and turn it into an array
		const data = await collection.find().toArray();
		res.send(JSON.stringify(data));
	        client.close();
	});
});

app.use(express.json());
app.use(express.urlencoded());
app.post('/process', (req, res) => {
    console.log(`Post data: \n ${req.body}`);
    console.log(req.body);
    client.connect(async err => {
	const collection = client.db("test").collection("devices");
	try {
	collection.insertOne(req.body);
	} catch (e) {
	    print (e);
	}
    });

});

app.listen(PORT, ()=>{ //listen to the port we chose above
    //print to the console that the server is listening
    console.log("listening to port: " + PORT);
});
