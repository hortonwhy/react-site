import "./App.css";
import Home from "./pages/Home";
import About from "./pages/About";
import Blog from "./pages/blog";
import leftCol from "./pages/leftCol";
import rightCol from "./pages/rightCol";
import CreatePost from "./pages/createPost";
import Login from "./pages/login";
import Nav from "./pages/Nav.js";
import { Route } from "react-router-dom";
import { StyleSheet, View } from "react-native";

function App() {
  return (
    <div>
      <View style={styles.container}>
        <View style={styles.outercolumns}>
          <Route path="/" component={leftCol} />
        </View>

        <View style={styles.bodyColumn}>
          {Nav()}
          <Route path="/" exact component={Home} />
          <Route path="/about" exact component={About} />
          <Route path="/blog" exact component={Blog} />
          <Route path="/createPost" exact component={CreatePost} />
          <Route path="/login" exact component={Login} />
        </View>

        <View style={styles.outercolumns}>
          <Route path="/" component={rightCol} />
        </View>
      </View>
    </div>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    //alignItems: 'center',
    //backgroundColor: '#19181A'
    backgroundColor: "grey",
    height: "100vh",
  },
  outercolumns: {
    textAlign: "center",
    width: "15%",
  },
  bodyColumn: {
    textAlign: "center",
    width: "70%",
  },
});

export default App;
