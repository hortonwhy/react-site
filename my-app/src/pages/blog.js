import '../css/Blog.css';
import myApi from './api';
import { useAsync } from 'react-async';
//const c = require('./mongo.js');

//const url = "https://jsonplaceholder.typicode.com/users";
//const myApi = async () =>
    //const url = "http://localhost:5001";
 //   await fetch(url)
//	.then(response => response.ok ? response: Promise.reject(response))
//	.then(response => response.json());


function Blog () {
    const { data, error, isLoading } = new useAsync({ promiseFn: myApi });
    if (isLoading) return "loading...";
    if (error) return `something went wrong: ${error.message}`;
    if (data);
    console.log(data);
   return (
       <div>
	   <h1> Blog Data: </h1>
	   {
	   data.map(
	       (post, key)=>{
		   return(
			   <div key={post._id}>
			   <h3 class="author">
			   Author: {post.author}
		       </h3>
			   <h2 class="title">
			   Title: {post.title}
			   </h2>

		       Body: <p class="body">
			   {post.body}
		       </p>


		           </div>); })
       }
       </div>
   );
}    


export default Blog;
