// https://css-tricks.com/fetching-data-in-react-using-react-async/
const url = "http://localhost:5001";
const myApi = async () =>
    await fetch(url)
	.then(response => response.ok ? response: Promise.reject(response))
	.then(response => response.json());
	     
export default myApi;
