import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import github_logo from "../imgs/GitHub-Mark-32px.png";
import gitlab_logo from "../imgs/gitlab-logo.png";

function Nav() {
  const [scrolled, setScrolled] = useState(false);
  const handleScroll = () => {
    const offset = window.scrollY;
    if (offset > 100) {
      setScrolled(true);
      //console.log("scrolled");
    } else {
      setScrolled(false);
    }
  };
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  });

  let navbarClasses = ["nav"];
  if (scrolled) {
    navbarClasses.push("scrolled");
  }

  return (
    <div className={navbarClasses.join(" ")}>
      <table>
        <thead>
          <tr>
            <th>
              <Link to="/">Home</Link>
            </th>
            <th>
              <Link to="/blog">Blog</Link>
            </th>
            <th>
              <Link to="/about">About me</Link>
            </th>
          </tr>
        </thead>
        <thead id="navlinks-th">
          <tr>
            <th>
              <a
                href="https://github.com/hortonwhy"
                target="_blank"
                rel="noreferrer"
                class="icon-img-ctn"
              >
                <img
                  class="icon-img"
                  style={{ maxWidth: 16, minHeight: 0 }}
                  src={github_logo}
                  alt="github"
                />
              </a>
            </th>
            <th>
              <a
                href="https://gitlab.com/hortonwhy"
                target="_blank"
                rel="noreferrer"
                class="icon-img-ctn"
              >
                <img
                  class="icon-img"
                  style={{ maxWidth: 32, minHeight: 0 }}
                  src={gitlab_logo}
                  alt="github"
                />
              </a>
            </th>
          </tr>
        </thead>
      </table>
    </div>
  );
}

export default Nav;
