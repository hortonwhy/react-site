import { useState } from "react";

function CreatePost () {
    const [author, setAuthor] = useState("");
    const [title, setTitle] = useState("");
    const [body, setBody] = useState("");

    const handleSubmit = (event) => {
	// function to handle the data and submit event
	event.preventDefault();
	alert(`The name entered was: ${author}`);
	console.log("author", author, "title", title, "body", body);
	fetch('http://localhost:5001/process', {
	    method:  'post',
	    body: JSON.stringify({
		author: author,
		title: title,
		body: body,
	    }),
	    headers: {
		'Accept': 'application/json',
		'Content-type': 'application/json'
	    }
	});
    };

    return (
	    <div id="formBlog">
	    <h1>create post</h1>
	    <form onSubmit={handleSubmit}>
	    <label>
	    Enter Author name:
<input type="text" value={author} onChange={(e) => setAuthor(e.target.value)}/>
	    </label>
	    <label>
	    <br></br>

	    Enter title:
<input type="text" value={title} onChange={(e) => setTitle(e.target.value)}/>
	    </label>
	    <label>
	    <br></br>

	    Enter Body text:
<textarea value={body} onChange={(e) => setBody(e.target.value)}/>
	    </label>
	    <br></br>
	    <br></br>
	    <input type="submit" value="Submit" />
	    </form>
	    </div>

    );
}

export default CreatePost;
