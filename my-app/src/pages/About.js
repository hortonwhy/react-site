import me from "../imgs/pfp.jpeg";
import server from "../imgs/server-neofetch.png";
import server_docker from "../imgs/server-docker-ps.png";
import { Text, StyleSheet, Linking } from "react-native";

function About() {
  return (
    <div>
      <br></br>
      <br></br>
      <img
        class="circle-img"
        style={{ minWidth: 70, minHeight: 70 }}
        src={me}
        alt="me!"
      />
      <div class="about-me-ctn" style={{ maxWidth: 800 }}>
        <div class="about-me-body">
          <Text style={styles.baseText}>
            <Text style={styles.titleText}>Hi! I am Wyatt</Text>
            {"\n"}
            {"\n"}
            <Text numberOfLines={10}>
              I am a new grad Japanese student who minored in Computer science.
              My passions include reading, cooking, rocking climbing,
              exercising, and coding. I am currently trying to learn react and
              do more web development projects. I want to improve my rock
              climbing abilities, and recently I have been enjoying reading
              about philosphy. My favorite code editor and OS is emacs and
              linux, respectively
            </Text>
            {"\n"}
            <Text style={styles.titleText}>About this website</Text>
            <Text numberOfLines={10}>
              This website is hosted on my own personal server rack. Its running
              containerized in docker and can be managed by me whenever I ssh
              into the machine. I made this to practice my front-end skills
              without relying on too many libraries like flutter and bootstrap
              to style for me. The website's code is hosted on gitlab {"\n"}
              <Text
                style={styles.LinkStyle}
                onPress={() =>
                  Linking.openURL(
                    "https://gitlab.com/hortonwhy/react-site/-/tree/main"
                  )
                }
              >
                https://gitlab.com/hortonwhy/react-site/-/tree/main
              </Text>
            </Text>
            {"\n"}
            <img
              style={{ minWidth: 50, minHeight: 50, maxWidth: 800 }}
              src={server}
              alt="me!"
            />
            {"\n"}
            <img
              style={{ minWidth: 50, minHeight: 50, maxWidth: 800 }}
              src={server_docker}
              alt="me!"
            />
          </Text>
        </div>
      </div>
    </div>
  );
}

const styles = StyleSheet.create({
  baseText: {
    font: "Ubuntu",
    fontSize: 20,
  },
  titleText: {
    fontSize: 28,
    fontWeight: "bold",
  },
  LinkStyle: {
    textDecorationLine: "underline",
  },
});

export default About;
